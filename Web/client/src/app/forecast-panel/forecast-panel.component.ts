import { Component, OnInit } from '@angular/core';
import { ForecastService } from '../shared/forecast/forecast.service';
import { PlaceService } from '../shared/place/place.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-forecast-panel',
  templateUrl: './forecast-panel.component.html',
  styleUrls: ['./forecast-panel.component.css']
})
export class ForecastPanelComponent implements OnInit{

  dayForecast : object;
  nightForecast : object;
  activeForecast : object = {};
  availableDates: Array<any>;
  dayForecastActive : Boolean;
  locations: Array<String>;
  searchBox = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  selectedDate: object;
  location : String;
  autoPanel = new FormControl();
  searchText : {};

  constructor(private forecastService: ForecastService, private placeService: PlaceService) { }
  ngOnInit() {

    this.filteredOptions = this.searchBox.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );


    this.dayForecastActive=true;
    this.forecastService.getAvailableDates().subscribe(dates => {
      this.availableDates = dates;
    });

    this.placeService.getPlacesWithActiveForecasts().subscribe(locations=>{
      this.options = locations;
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  SwitchToGeneralForecast(){
    this.GetNewGeneralForecastData();
  }

  LocationChanged(location){
    this.location = location;
    this.placeService.getForecastByDateAndLocation(this.selectedDate, location.value).subscribe(data=>{

        if(data.day === null || data.night === null){
          this.activeForecast = {};
          this.dayForecast = {};
          this.nightForecast = {};
          return;
        }


        if(this.dayForecastActive){
            this.activeForecast=data.day;
          }else{
            this.activeForecast=data.night;
          }
          this.dayForecast = data.day;
          this.nightForecast = data.night;
    });
  }

  ChangeForecastType = (): void => {
    if(this.activeForecast===this.dayForecast){
      this.activeForecast=this.nightForecast;
      this.dayForecastActive = false;
    }else{
      this.activeForecast=this.dayForecast;
      this.dayForecastActive = true;
    }
  }
  
  DateTabChanged = (tabChangeEvent): void => {
    this.selectedDate = this.availableDates[tabChangeEvent.index];

    if (this.searchText) {
      this.LocationChanged(this.location);
      return;
    }
    this.GetNewGeneralForecastData();
  }


  GetNewGeneralForecastData(){
      this.searchText='';
      this.forecastService.getByDate(this.selectedDate).subscribe(data => {
        if(this.dayForecastActive){
          this.activeForecast=data.day;
        }else{
          this.activeForecast=data.night;
        }
        this.dayForecast = data.day;
        this.nightForecast = data.night;
      });

  }
}





