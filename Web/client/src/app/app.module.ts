import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PlaceService } from './shared/place/place.service';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule,MatAutocompleteModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ForecastService } from './shared/forecast/forecast.service';
import { ForecastPanelComponent } from './forecast-panel/forecast-panel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ForecastPanelComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatTabsModule,
    BrowserModule,
    MatInputModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [PlaceService,ForecastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
