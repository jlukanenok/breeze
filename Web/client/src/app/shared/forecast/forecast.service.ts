import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ForecastService {

  constructor(private http: HttpClient) {
  }

  getAvailableDates(): Observable<any> {
  	//return null;
    return this.http.get('//localhost:8080/forecast/get-available-dates');
  }

  getByDate(date): Observable<any> {
    return this.http.get('//localhost:8080/forecast/get-by-date/'+date);
  }
}