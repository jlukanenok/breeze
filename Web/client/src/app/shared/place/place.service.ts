import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PlaceService {

  constructor(private http: HttpClient) {
  }

  getPlacesWithActiveForecasts(): Observable<any> {
    return this.http.get('//localhost:8080/place/get-active-places');
  }

  getForecastByDateAndLocation(date, location): Observable<any> {
    return this.http.get('//localhost:8080/place/get-forecast/'+date+'&'+location);
  }
}