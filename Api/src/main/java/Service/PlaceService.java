package Service;

import Db.Repositories.PlaceRepository;
import Models.Forecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class PlaceService {


    @Autowired
    private PlaceRepository placeRepository;

    public PlaceService(PlaceRepository placeRepository){
        this.placeRepository=placeRepository;
    }

    public ArrayList<String> GetPlacesWithActiveForecasts(){
        return placeRepository.GetPlacesWithActiveForecasts();
    }

    public Forecast GetForecastForPlace(String place, Date date){
        return placeRepository.GetForecastForPlace(place, date);
    }

}
