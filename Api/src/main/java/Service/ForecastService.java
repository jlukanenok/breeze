package Service;

import Db.Repositories.ForecastRepository;
import Models.Forecast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class ForecastService{

    @Autowired
    private ForecastRepository forecastRepository;

    public ForecastService(ForecastRepository forecastRepository) {
        this.forecastRepository=forecastRepository;
    }

    public ArrayList<Forecast> GetLastForecasts() {
        return null;
    }

    public ArrayList<Date> GetAvailableForecastDates() {
        return forecastRepository.GetAvailableForecastDates();
    }

    public Forecast GetAvailableForecastForDate(Date date) {
        return forecastRepository.GetForecastByDate(date);
    }
}
