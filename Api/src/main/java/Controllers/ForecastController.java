package Controllers;


import Models.Forecast;
import Service.ForecastService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/forecast")
@CrossOrigin(origins = "http://localhost:4200")
public class ForecastController {

    Logger logger = LoggerFactory.getLogger(ForecastController.class);

    @Autowired
    ForecastService forecastService;

    @Autowired
    public ForecastController(ForecastService forecastService){
        this.forecastService=forecastService;
    }

    @GetMapping("/get-available-dates")
    public ArrayList<Date> GetAvailableForecastDates() {
        return forecastService.GetAvailableForecastDates();
    }


    @GetMapping("/get-by-date/{date}")
    public Forecast GetForecastByDate(@PathVariable("date")String date) {

        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date forecastDate = format.parse(date);
            return forecastService.GetAvailableForecastForDate(forecastDate);

        }catch(ParseException e){
            logger.warn("Unable to parse date /get-by-date/"+e.toString());
            return null;
        }
    }

}
