package Controllers;

import Models.Forecast;
import Service.PlaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/place")
@CrossOrigin(origins = "http://localhost:4200")
public class PlaceController {

    @Autowired
    private PlaceService placeService;

    Logger logger = LoggerFactory.getLogger(PlaceController.class);


    public PlaceController(PlaceService placeService){
        this.placeService = placeService;
    }

    @GetMapping("/get-active-places")
    public List<String> GetPlacesWithFutureForecasts() {
        return placeService.GetPlacesWithActiveForecasts();
    }

    @GetMapping("/get-forecast/{date}&{place}")
    public Forecast GetForecastForPlace(@PathVariable("date")String date,@PathVariable("place")String place) {
        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date forecastDate = format.parse(date);
            return placeService.GetForecastForPlace(place,forecastDate);
        }catch(ParseException e){
            logger.warn("Unable to parse date /get-forecast/"+e.toString());
            return null;
        }
    }
}
