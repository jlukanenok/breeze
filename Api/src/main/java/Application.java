import Db.DbInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication(scanBasePackages = {"Service", "Application", "Downloader","Controllers", "Db"})
@EnableScheduling
public class Application {

    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(Application.class);
        if(new DbInitializer().ConnectToDb()){
            SpringApplication.run(Application.class, args);
            logger.info("Application started successfully");
        }else{
            logger.error("There were some errors! Application did not start!");
        }
    }
}
