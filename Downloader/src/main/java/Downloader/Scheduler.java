package Downloader;

import Db.Repositories.BaseRepository;
import Models.Forecasts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXB;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.Arrays;

@Component
public class Scheduler {

    private static final Logger log = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    private BaseRepository baseRepository;

    public Scheduler(BaseRepository baseRepository){
        this.baseRepository=baseRepository;
    }

    @Scheduled(fixedRate = 1000*60*30)
    public void DownloadAndSaveForecast() {
        log.info("Scheduler started " + new Timestamp(System.currentTimeMillis()));
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange("http://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php?lang=eng", HttpMethod.GET,entity, String.class);
        String responseBody= result.getBody();
        try{
            Forecasts latestForecasts = JAXB.unmarshal(new StringReader(responseBody), Forecasts.class);
            baseRepository.SaveNewForecasts(latestForecasts);
        }catch(Exception e) {
            log.error("Unable to parse downloaded XML" + e.toString());
        }
    }
}