
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3190 (class 0 OID 0)
-- Dependencies: 3189
-- Name: DATABASE weather; Type: COMMENT; Schema: -; Owner: weather
--
COMMENT ON DATABASE weather IS 'no comments needed';
SET default_tablespace = '';
SET default_with_oids = false;
--
-- TOC entry 199 (class 1259 OID 16731)
-- Name: DayType; Type: TABLE; Schema: public; Owner: weather
--
CREATE TABLE public."DayType" (
    "TypeName" text NOT NULL,
    "ID" integer NOT NULL
);


ALTER TABLE public."DayType" OWNER TO weather;

--
-- TOC entry 198 (class 1259 OID 16729)
-- Name: DayType_ID_seq; Type: SEQUENCE; Schema: public; Owner: weather
--

CREATE SEQUENCE public."DayType_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DayType_ID_seq" OWNER TO weather;

--
-- TOC entry 3191 (class 0 OID 0)
-- Dependencies: 198
-- Name: DayType_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: weather
--

ALTER SEQUENCE public."DayType_ID_seq" OWNED BY public."DayType"."ID";


--
-- TOC entry 197 (class 1259 OID 16718)
-- Name: Forecast; Type: TABLE; Schema: public; Owner: weather
--

CREATE TABLE public."Forecast" (
    "ID" integer NOT NULL,
    "Phenomenon" text,
    "Date" date NOT NULL,
    "TempMin" numeric,
    "TempMax" numeric NOT NULL,
    "DayType" integer NOT NULL,
    "Text" text NOT NULL,
    "CreatedDate" timestamp without time zone NOT NULL
);

ALTER TABLE public."Forecast" OWNER TO weather;

--
-- TOC entry 196 (class 1259 OID 16716)
-- Name: Forecast_ID_seq; Type: SEQUENCE; Schema: public; Owner: weather
--

CREATE SEQUENCE public."Forecast_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Forecast_ID_seq" OWNER TO weather;

--
-- TOC entry 3192 (class 0 OID 0)
-- Dependencies: 196
-- Name: Forecast_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: weather
--

ALTER SEQUENCE public."Forecast_ID_seq" OWNED BY public."Forecast"."ID";

--
-- TOC entry 207 (class 1259 OID 16777)
-- Name: PeipsiForecast; Type: TABLE; Schema: public; Owner: weather
--

CREATE TABLE public."PeipsiForecast" (
    "ID" integer NOT NULL,
    "Text" text NOT NULL,
    "ForecastID" integer NOT NULL
);

ALTER TABLE public."PeipsiForecast" OWNER TO weather;

--
-- TOC entry 206 (class 1259 OID 16775)
-- Name: PeipsiForecast_ID_seq; Type: SEQUENCE; Schema: public; Owner: weather
--

CREATE SEQUENCE public."PeipsiForecast_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PeipsiForecast_ID_seq" OWNER TO weather;

--
-- TOC entry 3193 (class 0 OID 0)
-- Dependencies: 206
-- Name: PeipsiForecast_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: weather
--

ALTER SEQUENCE public."PeipsiForecast_ID_seq" OWNED BY public."PeipsiForecast"."ID";


--
-- TOC entry 201 (class 1259 OID 16744)
-- Name: PlaceForecast; Type: TABLE; Schema: public; Owner: weather
--

CREATE TABLE public."PlaceForecast" (
    "Location" text NOT NULL,
    "ID" integer NOT NULL,
    "TempMin" numeric,
    "ForecastID" integer NOT NULL,
    "Phenomenon" text,
    "TempMax" numeric
);


ALTER TABLE public."PlaceForecast" OWNER TO weather;

--
-- TOC entry 200 (class 1259 OID 16742)
-- Name: PlaceForecast_ID_seq; Type: SEQUENCE; Schema: public; Owner: weather
--

CREATE SEQUENCE public."PlaceForecast_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PlaceForecast_ID_seq" OWNER TO weather;

--
-- TOC entry 3194 (class 0 OID 0)
-- Dependencies: 200
-- Name: PlaceForecast_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: weather
--

ALTER SEQUENCE public."PlaceForecast_ID_seq" OWNED BY public."PlaceForecast"."ID";


--
-- TOC entry 205 (class 1259 OID 16766)
-- Name: SeaForecast; Type: TABLE; Schema: public; Owner: weather
--

CREATE TABLE public."SeaForecast" (
    "ForecastID" integer NOT NULL,
    "Text" text NOT NULL,
    "ID" integer NOT NULL
);


ALTER TABLE public."SeaForecast" OWNER TO weather;

--
-- TOC entry 204 (class 1259 OID 16764)
-- Name: SeaForecast_ID_seq; Type: SEQUENCE; Schema: public; Owner: weather
--

CREATE SEQUENCE public."SeaForecast_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SeaForecast_ID_seq" OWNER TO weather;

--
-- TOC entry 3195 (class 0 OID 0)
-- Dependencies: 204
-- Name: SeaForecast_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: weather
--

ALTER SEQUENCE public."SeaForecast_ID_seq" OWNED BY public."SeaForecast"."ID";


--
-- TOC entry 203 (class 1259 OID 16755)
-- Name: WindForecast; Type: TABLE; Schema: public; Owner: weather
--

CREATE TABLE public."WindForecast" (
    "Location" text NOT NULL,
    "ID" integer NOT NULL,
    "Direction" text NOT NULL,
    "ForecastID" integer NOT NULL,
    "SpeedMin" numeric,
    "SpeedMax" numeric
);


ALTER TABLE public."WindForecast" OWNER TO weather;

--
-- TOC entry 202 (class 1259 OID 16753)
-- Name: WindForecast_ID_seq; Type: SEQUENCE; Schema: public; Owner: weather
--

CREATE SEQUENCE public."WindForecast_ID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."WindForecast_ID_seq" OWNER TO weather;

--
-- TOC entry 3196 (class 0 OID 0)
-- Dependencies: 202
-- Name: WindForecast_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: weather
--

ALTER SEQUENCE public."WindForecast_ID_seq" OWNED BY public."WindForecast"."ID";


--
-- TOC entry 3039 (class 2604 OID 16734)
-- Name: DayType ID; Type: DEFAULT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."DayType" ALTER COLUMN "ID" SET DEFAULT nextval('public."DayType_ID_seq"'::regclass);


--
-- TOC entry 3038 (class 2604 OID 16721)
-- Name: Forecast ID; Type: DEFAULT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."Forecast" ALTER COLUMN "ID" SET DEFAULT nextval('public."Forecast_ID_seq"'::regclass);


--
-- TOC entry 3043 (class 2604 OID 16780)
-- Name: PeipsiForecast ID; Type: DEFAULT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."PeipsiForecast" ALTER COLUMN "ID" SET DEFAULT nextval('public."PeipsiForecast_ID_seq"'::regclass);


--
-- TOC entry 3040 (class 2604 OID 16747)
-- Name: PlaceForecast ID; Type: DEFAULT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."PlaceForecast" ALTER COLUMN "ID" SET DEFAULT nextval('public."PlaceForecast_ID_seq"'::regclass);


--
-- TOC entry 3042 (class 2604 OID 16769)
-- Name: SeaForecast ID; Type: DEFAULT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."SeaForecast" ALTER COLUMN "ID" SET DEFAULT nextval('public."SeaForecast_ID_seq"'::regclass);


--
-- TOC entry 3041 (class 2604 OID 16758)
-- Name: WindForecast ID; Type: DEFAULT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."WindForecast" ALTER COLUMN "ID" SET DEFAULT nextval('public."WindForecast_ID_seq"'::regclass);


--
-- TOC entry 3047 (class 2606 OID 16741)
-- Name: DayType DayType_TypeName_key; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."DayType"
    ADD CONSTRAINT "DayType_TypeName_key" UNIQUE ("TypeName");


--
-- TOC entry 3049 (class 2606 OID 16739)
-- Name: DayType daytype_pk; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."DayType"
    ADD CONSTRAINT daytype_pk PRIMARY KEY ("ID");


--
-- TOC entry 3045 (class 2606 OID 16726)
-- Name: Forecast forecast_pk; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."Forecast"
    ADD CONSTRAINT forecast_pk PRIMARY KEY ("ID");


--
-- TOC entry 3057 (class 2606 OID 16785)
-- Name: PeipsiForecast peipsiforecast_pk; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."PeipsiForecast"
    ADD CONSTRAINT peipsiforecast_pk PRIMARY KEY ("ID");


--
-- TOC entry 3051 (class 2606 OID 16752)
-- Name: PlaceForecast placeforecast_pk; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."PlaceForecast"
    ADD CONSTRAINT placeforecast_pk PRIMARY KEY ("ID");


--
-- TOC entry 3055 (class 2606 OID 16774)
-- Name: SeaForecast seaforecast_pk; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."SeaForecast"
    ADD CONSTRAINT seaforecast_pk PRIMARY KEY ("ID");


--
-- TOC entry 3053 (class 2606 OID 16763)
-- Name: WindForecast windforecast_pk; Type: CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."WindForecast"
    ADD CONSTRAINT windforecast_pk PRIMARY KEY ("ID");


--
-- TOC entry 3058 (class 2606 OID 16786)
-- Name: Forecast Forecast_fk0; Type: FK CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."Forecast"
    ADD CONSTRAINT "Forecast_fk0" FOREIGN KEY ("DayType") REFERENCES public."DayType"("ID");


--
-- TOC entry 3062 (class 2606 OID 16806)
-- Name: PeipsiForecast PeipsiForecast_fk0; Type: FK CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."PeipsiForecast"
    ADD CONSTRAINT "PeipsiForecast_fk0" FOREIGN KEY ("ForecastID") REFERENCES public."Forecast"("ID");


--
-- TOC entry 3059 (class 2606 OID 16791)
-- Name: PlaceForecast PlaceForecast_fk0; Type: FK CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."PlaceForecast"
    ADD CONSTRAINT "PlaceForecast_fk0" FOREIGN KEY ("ForecastID") REFERENCES public."Forecast"("ID");


--
-- TOC entry 3061 (class 2606 OID 16801)
-- Name: SeaForecast SeaForecast_fk0; Type: FK CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."SeaForecast"
    ADD CONSTRAINT "SeaForecast_fk0" FOREIGN KEY ("ForecastID") REFERENCES public."Forecast"("ID");


--
-- TOC entry 3060 (class 2606 OID 16796)
-- Name: WindForecast WindForecast_fk0; Type: FK CONSTRAINT; Schema: public; Owner: weather
--

ALTER TABLE ONLY public."WindForecast"
    ADD CONSTRAINT "WindForecast_fk0" FOREIGN KEY ("ForecastID") REFERENCES public."Forecast"("ID");

INSERT INTO public."DayType" VALUES ('Day', 1);
INSERT INTO public."DayType" VALUES ('Night', 2);