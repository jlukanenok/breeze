/*
 * This file is generated by jOOQ.
*/
package Breeze.generated.tables;


import Breeze.generated.Keys;
import Breeze.generated.Public;
import Breeze.generated.tables.records.PlaceforecastRecord;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.8"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Placeforecast extends TableImpl<PlaceforecastRecord> {

    private static final long serialVersionUID = -419513156;

    /**
     * The reference instance of <code>public.PlaceForecast</code>
     */
    public static final Placeforecast PLACEFORECAST = new Placeforecast();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PlaceforecastRecord> getRecordType() {
        return PlaceforecastRecord.class;
    }

    /**
     * The column <code>public.PlaceForecast.Location</code>.
     */
    public final TableField<PlaceforecastRecord, String> LOCATION = createField("Location", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>public.PlaceForecast.ID</code>.
     */
    public final TableField<PlaceforecastRecord, Integer> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('\"PlaceForecast_ID_seq\"'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.PlaceForecast.TempMin</code>.
     */
    public final TableField<PlaceforecastRecord, BigDecimal> TEMPMIN = createField("TempMin", org.jooq.impl.SQLDataType.NUMERIC, this, "");

    /**
     * The column <code>public.PlaceForecast.ForecastID</code>.
     */
    public final TableField<PlaceforecastRecord, Integer> FORECASTID = createField("ForecastID", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>public.PlaceForecast.Phenomenon</code>.
     */
    public final TableField<PlaceforecastRecord, String> PHENOMENON = createField("Phenomenon", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.PlaceForecast.TempMax</code>.
     */
    public final TableField<PlaceforecastRecord, BigDecimal> TEMPMAX = createField("TempMax", org.jooq.impl.SQLDataType.NUMERIC, this, "");

    /**
     * Create a <code>public.PlaceForecast</code> table reference
     */
    public Placeforecast() {
        this(DSL.name("PlaceForecast"), null);
    }

    /**
     * Create an aliased <code>public.PlaceForecast</code> table reference
     */
    public Placeforecast(String alias) {
        this(DSL.name(alias), PLACEFORECAST);
    }

    /**
     * Create an aliased <code>public.PlaceForecast</code> table reference
     */
    public Placeforecast(Name alias) {
        this(alias, PLACEFORECAST);
    }

    private Placeforecast(Name alias, Table<PlaceforecastRecord> aliased) {
        this(alias, aliased, null);
    }

    private Placeforecast(Name alias, Table<PlaceforecastRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<PlaceforecastRecord, Integer> getIdentity() {
        return Keys.IDENTITY_PLACEFORECAST;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<PlaceforecastRecord> getPrimaryKey() {
        return Keys.PLACEFORECAST_PK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<PlaceforecastRecord>> getKeys() {
        return Arrays.<UniqueKey<PlaceforecastRecord>>asList(Keys.PLACEFORECAST_PK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<PlaceforecastRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<PlaceforecastRecord, ?>>asList(Keys.PLACEFORECAST__PLACEFORECAST_FK0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Placeforecast as(String alias) {
        return new Placeforecast(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Placeforecast as(Name alias) {
        return new Placeforecast(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Placeforecast rename(String name) {
        return new Placeforecast(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Placeforecast rename(Name name) {
        return new Placeforecast(name, null);
    }
}
