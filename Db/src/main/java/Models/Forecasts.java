package Models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "forecasts")
public class Forecasts {
    private Forecast[] Forecast;

    public Forecast[] getForecast ()
    {
        return Forecast;
    }

    public void setForecast (Forecast[] forecast)
    {
        this.Forecast = forecast;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [forecast = "+Forecast+"]";
    }
}