package Models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Place {
    private String phenomenon;

    private Double tempmin;

    private Double tempmax;

    private String name;

    public String getPhenomenon ()
    {
        return phenomenon;
    }

    public void setPhenomenon (String phenomenon)
    {
        this.phenomenon = phenomenon;
    }

    public Double getTempmin ()
    {
        return tempmin;
    }

    public void setTempmin (Double tempmin)
    {
        this.tempmin = tempmin;
    }

    public Double getTempmax ()
    {
        return tempmax;
    }

    public void setTempmax (Double tempmax)
    {
        this.tempmax = tempmax;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }
}
