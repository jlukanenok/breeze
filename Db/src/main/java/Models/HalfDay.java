package Models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class HalfDay {
    private String phenomenon;

    private String tempmax;

    private String tempmin;

    private Wind[] wind;

    private String peipsi;

    private String text;

    private String sea;

    private Place[] place;

    public String getPhenomenon ()
    {
        return phenomenon;
    }

    public void setPhenomenon (String phenomenon)
    {
        this.phenomenon = phenomenon;
    }

    public String getTempmax ()
    {
        return tempmax;
    }

    public void setTempmax (String tempmax)
    {
        this.tempmax = tempmax;
    }

    public String getTempmin ()
    {
        return tempmin;
    }

    public void setTempmin (String tempmin)
    {
        this.tempmin = tempmin;
    }

    public Wind[] getWind ()
    {
        return wind;
    }

    public void setWind (Wind[] wind)
    {
        this.wind = wind;
    }

    public String getPeipsi ()
    {
        return peipsi;
    }

    public void setPeipsi (String peipsi)
    {
        this.peipsi = peipsi;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getSea ()
    {
        return sea;
    }

    public void setSea (String sea)
    {
        this.sea = sea;
    }

    public Place[] getPlace ()
    {
        return place;
    }

    public void setPlace (Place[] place)
    {
        this.place = place;
    }
}
