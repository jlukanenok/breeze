package Models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Wind
{
    private String gust;

    private String direction;

    private String name;

    private Double speedmax;

    private Double speedmin;

    public String getGust ()
    {
        return gust;
    }

    public void setGust (String gust)
    {
        this.gust = gust;
    }

    public String getDirection ()
    {
        return direction;
    }

    public void setDirection (String direction)
    {
        this.direction = direction;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Double getSpeedmax ()
    {
        return speedmax;
    }

    public void setSpeedmax (Double speedmax)
    {
        this.speedmax = speedmax;
    }

    public Double getSpeedmin ()
    {
        return speedmin;
    }

    public void setSpeedmin (Double speedmin)
    {
        this.speedmin = speedmin;
    }
}
