package Db;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.jooq.impl.DSL.count;

@Component
public class DbInitializer {

    final static String DATABASE_URL = "jdbc:postgresql://localhost:5432/weather";
    DSLContext context = null;
    Logger logger = LoggerFactory.getLogger(DbInitializer.class);



    public boolean ConnectToDb(){
        try{
            Connection connection = DriverManager.getConnection(DATABASE_URL, "weather", "weather");
            context = DSL.using(connection, SQLDialect.POSTGRES);
            context.createSchemaIfNotExists("public").execute();
            if(!IsDbInitialized()){
                InitializeDb();
            }
            return true;
        }catch(Exception e){
            logger.error("Unable to connect to db");
            return false;
        }
    }

    private boolean IsDbInitialized(){
        int numberOfTables =
                context
                        .select(count())
                        .from("information_schema.tables")
                        .where("table_schema = 'public'")
                        .fetchOne(0, int.class);

        if(numberOfTables>0)
            return true;
        else
            return false;
    }

    private void InitializeDb() {

        try{
            Path path = Paths.get(getClass().getClassLoader()
                    .getResource("db_generating_script.sql").toURI());

            Stream<String> fileLines = Files.lines(path);
            String sqlScript = fileLines.collect(Collectors.joining("\n"));
            fileLines.close();
            logger.info("Schema is missing, generating new one");
            context.execute(sqlScript);

        }catch(Exception e){
            logger.error("Unable to generate schema: "+e.toString());
        }
    }

}
