package Db;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBCConnection {

    protected final String DATABASE_URL = "jdbc:postgresql://localhost:5432/weather";
    protected  Connection connection = null;
    protected DSLContext context = null;

    public JDBCConnection(){
        Logger logger = LoggerFactory.getLogger(JDBCConnection.class);
        try{
            connection = DriverManager.getConnection(DATABASE_URL, "weather", "weather");
            context = DSL.using(connection, SQLDialect.POSTGRES);
            context.createSchemaIfNotExists("public").execute();
        }catch(Exception e){
            logger.error("Unable to connect to db: "+e.toString());
        }
    }

}
