package Db.Repositories;

import Db.JDBCConnection;
import Models.Forecast;
import Models.Forecasts;
import Models.Place;
import Models.Wind;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneOffset;

import static Breeze.generated.Tables.*;

@Component
public class BaseRepository extends JDBCConnection {

    public void SaveNewForecasts (Forecasts forecasts){

        if(forecasts != null && forecasts.getForecast().length>0){

            for (Forecast forecast: forecasts.getForecast()) {


                //inserting general forecast
                Record dayForecastRecord = SaveGeneralForecast(forecast, true);
                Record nightForecastRecord = SaveGeneralForecast(forecast, false);

                Integer nightForecastID = nightForecastRecord.getValue(FORECAST.ID);
                Integer dayForecastID = dayForecastRecord.getValue(FORECAST.ID);


                //inserting peipsi data
                if(forecast.getDay().getPeipsi() != null && !forecast.getDay().getPeipsi().isEmpty()){
                    SavePeipsiData(forecast.getNight().getPeipsi(),dayForecastID);
                }
                if(forecast.getNight().getPeipsi() != null && !forecast.getNight().getPeipsi().isEmpty()){
                    SavePeipsiData(forecast.getNight().getPeipsi(),nightForecastID);
                }

                //inserting places data
                if(forecast.getDay().getPlace() != null){
                    SaveDayPlacesData(forecast.getDay().getPlace(),dayForecastID);
                }
                if(forecast.getNight().getPlace() != null){
                    SaveNightPlacesData(forecast.getNight().getPlace(),nightForecastID);
                }


                //inserting sea data
                if(forecast.getDay().getSea() != null){
                    SaveSeaData(forecast.getDay().getSea(), dayForecastID);
                }
                if(forecast.getNight().getSea() != null) {
                    SaveSeaData(forecast.getNight().getSea(), nightForecastID);
                }

                //inserting wind data
                if(forecast.getDay().getWind() != null){
                    SaveWindData(forecast.getDay().getWind(), dayForecastID);
                }
                if(forecast.getNight().getWind() != null) {
                    SaveWindData(forecast.getNight().getWind(), nightForecastID);
                }

            }
        }
    }

    private Record SaveGeneralForecast(Forecast forecast, boolean isDayForecast){

        LocalDate forecastDate = LocalDate.parse(forecast.getDate());
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        return context.insertInto(FORECAST,
                FORECAST.DATE,
                FORECAST.PHENOMENON,
                FORECAST.TEMPMIN,
                FORECAST.TEMPMAX,
                FORECAST.DAYTYPE,
                FORECAST.TEXT,
                FORECAST.CREATEDDATE)
                .values(
                        new Date(forecastDate.atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli()),
                        forecast.getNight().getPhenomenon(),
                        new BigDecimal(forecast.getNight().getTempmin()),
                        new BigDecimal(forecast.getNight().getTempmax()),
                        isDayForecast ? 1 : 2,
                        forecast.getNight().getText(),
                        currentTime
                ).returning(FORECAST.ID)
                .fetchOne();
    }

    private void SavePeipsiData(String text, int forecastID){
        context.insertInto(PEIPSIFORECAST,
                PEIPSIFORECAST.FORECASTID,
                PEIPSIFORECAST.TEXT)
                .values(forecastID,
                        text).execute();
    }

    private void SaveSeaData(String text, int forecastID){
        context.insertInto(SEAFORECAST,
                SEAFORECAST.FORECASTID,
                SEAFORECAST.TEXT)
                .values(forecastID,
                        text).execute();
    }

    private void SaveDayPlacesData(Place[] places, int forecastID){
        for(Place place : places){
            context.insertInto(PLACEFORECAST,
                    PLACEFORECAST.TEMPMAX,
                    PLACEFORECAST.PHENOMENON,
                    PLACEFORECAST.LOCATION,
                    PLACEFORECAST.FORECASTID)
                    .values(new BigDecimal(place.getTempmax()),
                            place.getPhenomenon(),
                            place.getName(),
                            forecastID).execute();
        }
    }

    private void SaveNightPlacesData(Place[] places, int forecastID){
        for(Place place : places){
            context.insertInto(PLACEFORECAST,
                    PLACEFORECAST.TEMPMIN,
                    PLACEFORECAST.PHENOMENON,
                    PLACEFORECAST.LOCATION,
                    PLACEFORECAST.FORECASTID)
                    .values(new BigDecimal(place.getTempmin()),
                            place.getPhenomenon(),
                            place.getName(),
                            forecastID).execute();
        }
    }

    private void SaveWindData(Wind[] winds, int forecastID){
        for(Wind wind : winds){
            context.insertInto(WINDFORECAST,
                    WINDFORECAST.LOCATION,
                    WINDFORECAST.DIRECTION,
                    WINDFORECAST.SPEEDMAX,
                    WINDFORECAST.SPEEDMIN,
                    WINDFORECAST.FORECASTID)
                    .values(wind.getName(),
                            wind.getDirection(),
                            new BigDecimal(wind.getSpeedmax()),
                            new BigDecimal(wind.getSpeedmin()),
                            forecastID).execute();
        }
    }
}
