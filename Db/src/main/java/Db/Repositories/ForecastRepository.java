package Db.Repositories;

import Db.JDBCConnection;
import Models.Day;
import Models.Forecast;
import Models.Night;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static Breeze.generated.tables.Forecast.FORECAST;

@Component
public class ForecastRepository extends JDBCConnection {

    public ArrayList<Date> GetAvailableForecastDates(){

        java.sql.Date todayDate = new java.sql.Date(new Date().getTime());
        Date[] dates = context.selectDistinct(FORECAST.DATE)
                .from(FORECAST)
                .where(FORECAST.DATE.greaterOrEqual(todayDate))
                .orderBy(FORECAST.DATE)
                .fetchArray(FORECAST.DATE);
        return new ArrayList<Date>(Arrays.asList(dates));
    }

    public Forecast GetForecastByDate(Date date){

        Day dayForecast = context.select()
                .from(FORECAST)
                .where(FORECAST.DATE.equal(new java.sql.Date(date.getTime()))).and(FORECAST.DAYTYPE.eq(1))
                .orderBy(FORECAST.CREATEDDATE.desc()).fetchAny().into(Day.class);

        Night nightForecast = context.select()
                .from(FORECAST)
                .where(FORECAST.DATE.equal(new java.sql.Date(date.getTime()))).and(FORECAST.DAYTYPE.eq(2))
                .orderBy(FORECAST.CREATEDDATE.desc()).fetchAny().into(Night.class);

        Forecast forecast = new Forecast();
        forecast.setNight(nightForecast);
        forecast.setDay(dayForecast);
        forecast.setDate(date.toString());
        return forecast;
    }
}
