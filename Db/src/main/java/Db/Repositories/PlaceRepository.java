package Db.Repositories;

import Db.JDBCConnection;
import Models.Day;
import Models.Forecast;
import Models.Night;
import org.jooq.Record;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static Breeze.generated.tables.Forecast.FORECAST;
import static Breeze.generated.tables.Placeforecast.PLACEFORECAST;

@Component
public class PlaceRepository  extends JDBCConnection {

    public ArrayList<String> GetPlacesWithActiveForecasts(){

        java.sql.Date todayDate = new java.sql.Date(new Date().getTime());
        String[] places = context.selectDistinct(PLACEFORECAST.LOCATION)
                .from(PLACEFORECAST)
                .join(FORECAST).on(FORECAST.ID.eq(PLACEFORECAST.FORECASTID))
                .where(FORECAST.DATE.greaterOrEqual(todayDate))
                .orderBy(PLACEFORECAST.LOCATION)
                .fetchArray(PLACEFORECAST.LOCATION);

        return new ArrayList<String>(Arrays.asList(places));

    }


    public Forecast GetForecastForPlace(String location, Date forecastDate){

        java.sql.Date forecastDateSqlDate = new java.sql.Date(forecastDate.getTime());
        Forecast forecast= new Forecast();
        Day dayForecast;
        Record dayForecastRecord = context.select(PLACEFORECAST.LOCATION,PLACEFORECAST.PHENOMENON,PLACEFORECAST.TEMPMAX)
                .from(PLACEFORECAST)
                .join(FORECAST).on(FORECAST.ID.eq(PLACEFORECAST.FORECASTID))
                .where(FORECAST.DATE.eq(forecastDateSqlDate))
                .and(FORECAST.DAYTYPE.eq(1))
                .and(PLACEFORECAST.LOCATION.eq(location))
                .orderBy(FORECAST.DATE.asc())
                .limit(1)
                .fetchOne();

        if(dayForecastRecord!=null){
            dayForecast= dayForecastRecord.into(Day.class);
            forecast.setDay(dayForecast);
        }


        Night nightForecast;
        Record nightForecastRecord = context.select(PLACEFORECAST.LOCATION,PLACEFORECAST.PHENOMENON,PLACEFORECAST.TEMPMIN)
                .from(PLACEFORECAST)
                .join(FORECAST).on(FORECAST.ID.eq(PLACEFORECAST.FORECASTID))
                .where(FORECAST.DATE.eq(forecastDateSqlDate))
                .and(FORECAST.DAYTYPE.eq(2))
                .and(PLACEFORECAST.LOCATION.eq(location))
                .orderBy(FORECAST.DATE.asc())
                .limit(1)
                .fetchOne();

        if(nightForecastRecord!=null){
            nightForecast= nightForecastRecord.into(Night.class);
            forecast.setNight(nightForecast);
        }

        forecast.setLocation(location);
        return forecast;

    }
}
